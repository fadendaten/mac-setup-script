def setHostname(prompt,short_comp)
  pc_number = prompt.ask("Welche PC-Nummer hat das Gerät? (0 - 999)") do |q|
    q.required true
    q.convert :string
  end

  pc_number = pc_number.rjust(3,"0")

  pc_name = short_comp.downcase + "-" + pc_number

  shellexec("sudo scutil --set ComputerName '#{pc_name}'","Computername konnte nicht festgelegt werden!")
  shellexec("sudo scutil --set HostName '#{pc_name}'","Hostname konnte nicht festgelegt werden!")
  shellexec("sudo scutil --set LocalHostName '#{pc_name}'","Lokaler Hostname konnte nicht festgelegt werden!")

  return pc_name
end
