def installPackages(preset)
  config_array = YAML.load_file("./configs/packages.yaml")

  required_taps = config_array[preset]["tap"]
  if required_taps
    for tap in required_taps do
      shellexec(%(brew tap #{package}), "Der tap #{tap} konnte dem System nicht hinzugefügt werden!")
      shellexec(%(brew tap-pin #{tap}), "Der tap #{tap} konnte nich angepinnt werden!")
    end
  end

  if config_array[preset]["packages"]
    for package in config_array[preset]["packages"] do
      shellexec("brew install #{package}", "Das Packet #{package} konnte nicht installiert werden!")
    end
  end

  if config_array[preset]["casks"]
    for cask in config_array[preset]["casks"] do
      shellexec("brew cask install #{cask}", "Der Cask #{cask} konnte nicht installiert werden!")
    end
  end

  if config_array[preset]["dock"]
    shellexec("dockutil --remove all --no-restart", "Es konnten nicht alle Icons gelöscht werden!")

    for dock in config_array[preset]["dock"] do
      shellexec("dockutil --add /Applications/#{dock}.app/ --no-restart", "Das Icon #{cask} konnte nicht ins Dock aufgenommen werden!")
    end

    shellexec("dockutil --add /Applications/ --view grid --display stack --sort name --no-restart", "Der App-Ordner konnte nicht ins Dock aufgenommen werden!")
    shellexec("dockutil --add ~/Downloads/ --view grid --display stack --sort dateadded", "Der Downloads-Ordner konnte nicht ins Dock aufgenommen werden!")
  end
end
