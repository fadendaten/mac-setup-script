#!/usr/bin/ruby

def shellexec(command, error, log = "log.txt")
  system "#{command} >> #{log}"
  if $?.exitstatus > 0
    puts "#{error}"
  end
end
