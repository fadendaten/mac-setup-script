def setCompanies(prompt)
  companies_array = YAML.load_file("./configs/companies.yaml")

  i = 0
  company = prompt.select("Zu welcher Firma gehört das Gerät?", per_page: 8, filter: true) do |menu|
    while i < companies_array["companies"].count do
      menu.choice companies_array["companies"][i]["company"]
      i += 1
    end
  end

  i = 0
  while i < companies_array["companies"].count do
    if companies_array["companies"][i]["company"] == company
      short_comp = companies_array["companies"][i]["short"]
    end
    i += 1
  end

  return short_comp
end
