def setLang(prompt)
  languages = YAML.load_file("./configs/lang.yaml")

  select_lang = prompt.select("Welche Sprache soll Konfiguriert werden?", per_page: 8, filter: true) do |menu|
    for lang in languages do
      menu.choice lang
    end
  end

  shellexec("defaults write NSGlobalDomain AppleLanguages -array #{select_lang}", "Die Sprache konnte nicht eingestellt werden!")

  return select_lang
end
